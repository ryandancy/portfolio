---
layout: inner
position: left
title: 'Variant Tap'
date: 2020-01-08 02:59:00
categories: development
tags: Java Android Google~Play
featured_image: '/img/posts/01-variant-tap.png'
project_link: 'https://gitlab.com/coppercoder/variant-tap'
google_play_link: 'http://play.google.com/store/apps/details?id=ca.keal.varianttap&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'
button_icon: 'fab fa-gitlab'
button_text: 'Visit Project'
lead_text: 'A simple and addictive Android game.'
---
