---
layout: inner
position: left
title: 'Library App'
date: 2017-08-04 08:00:00
categories: development
tags: JavaScript Node.js MongoDB RAML Express Mocha Chai
featured_image: '/img/posts/04-library-app.png'
project_link: 'https://gitlab.com/coppercoder/library-app'
button_icon: 'fab fa-gitlab'
button_text: 'Visit Project'
lead_text: 'A Node.js library management CRUD API with 700+ unit tests.'
---
